#include <cstdint>
#include <cstdio>
#include <vector>

extern "C"
{
#include <jinclude.h>
#include <jpeglib.h>
#include <jversion.h>
}

struct File
{
  File() noexcept
    : f(nullptr)
  {}

  ~File() noexcept
  {
    close();
  }

  void close() noexcept
  {
    if (f != nullptr)
    {
      fclose(f);
      f = nullptr;
    }
  }

  operator bool() const noexcept
  {
    return f != nullptr;
  }

  void open(const char* path, const char* mode) noexcept
  {
    close();

#ifdef _MSC_VER
    if (fopen_s(&f, path, mode))
    {
      f = nullptr;
    }
#else
    f = std::fopen(path, mode);
#endif
  }

  FILE* f;
};

int main(int argc, const char* const* args)
{
  if (argc < 2)
  {
    fprintf(stderr, "Input file is not specified.\n");
    return 1;
  }

  File inFile, outFile;
  const char* const inFilePath = args[1];
  inFile.open(inFilePath, "rb");

  if (!inFile)
  {
    fprintf(stderr, "Failed to open input file %s.\n", inFilePath);
    return 1;
  }

  const char* outFilePath;

  if (argc == 2)
  {
    // Transcode to the same file.
    outFilePath = inFilePath;
  }
  else
  {
    outFilePath = args[2];
    outFile.open(outFilePath, "wb");

    if (!outFile)
    {
      fprintf(stderr, "Failed to open output file %s.\n", outFilePath);
      return 1;
    }
  }

  jpeg_decompress_struct decompressor;
  jpeg_error_mgr         errorMgr;

  jpeg_create_decompress(&decompressor);
  jpeg_stdio_src(&decompressor, inFile.f);
  decompressor.err = jpeg_std_error(&errorMgr);

  const int res = jpeg_read_header(&decompressor, TRUE);

  if (res != JPEG_HEADER_OK)
  {
    fprintf(stderr, "Corrupted JPEG header.\n");
    jpeg_destroy_decompress(&decompressor);
    return 1;
  }

  const JDIMENSION width = decompressor.image_width;
  const JDIMENSION height = decompressor.image_height;
  const size_t scanlineSize = static_cast<size_t>(width) *
                              decompressor.num_components;
  std::vector<JSAMPLE> imgVec(scanlineSize * height);
  JSAMPLE* const imgPtr = imgVec.data();
  JSAMPLE* scanlinePtr = imgPtr;

  jpeg_start_decompress(&decompressor);

  for (JDIMENSION i = 0; i < height; )
  {
    if (jpeg_read_scanlines(&decompressor, &scanlinePtr, 1))
    {
      scanlinePtr += scanlineSize;
      ++i;
    }
    else
    {
      fprintf(stderr, "Decompression failed.\n");
      jpeg_finish_decompress(&decompressor);
      jpeg_destroy_decompress(&decompressor);
      return 1;
    }
  }

  jpeg_finish_decompress(&decompressor);
  jpeg_destroy_decompress(&decompressor);
  inFile.close();

  if (!outFile)
  {
    outFile.open(outFilePath, "wb");

    if (!outFile)
    {
      fprintf(stderr, "Failed to open file %s for writing.\n", outFilePath);
      return 1;
    }
  }

  jpeg_compress_struct compressor;
  jpeg_create_compress(&compressor);
  compressor.err = decompressor.err;
  jpeg_stdio_dest(&compressor, outFile.f);
  compressor.image_width = width;
  compressor.image_height = height;
  compressor.input_components = decompressor.num_components;
  compressor.in_color_space = decompressor.out_color_space;
  jpeg_set_defaults(&compressor);

  jpeg_start_compress(&compressor, TRUE);
  scanlinePtr = imgPtr;

  for (JDIMENSION i = 0; i < height; )
  {
    if (jpeg_write_scanlines(&compressor, &scanlinePtr, 1))
    {
      scanlinePtr += scanlineSize;
      ++i;
    }
    else
    {
      fprintf(stderr, "Compression failed.\n");
      jpeg_finish_compress(&compressor);
      jpeg_destroy_compress(&compressor);
      return 1;
    }
  }

  jpeg_finish_compress(&compressor);
  jpeg_destroy_compress(&compressor);

  return 0;
}
